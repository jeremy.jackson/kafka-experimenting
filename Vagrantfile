# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "bento/ubuntu-16.04"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
  end

  config.vm.provision "shell", inline: <<-SHELL
    apt-get update -qq
    apt-get install -y -qq openjdk-8-jre openjdk-8-jdk

    if ! ls /vagrant/kafka_2.11-1.1.0.tgz
    then
      wget -q -O /vagrant/kafka_2.11-1.1.0.tgz \
        http://apache.mirrors.pair.com/kafka/1.1.0/kafka_2.11-1.1.0.tgz
    fi

    tar xzf /vagrant/kafka_2.11-1.1.0.tgz
    cd kafka_2.11-1.1.0

    # Creating multiple brokers for this machine.
    echo "s/broker\\.id=0/broker.id=0/\ns/#listeners=PLAINTEXT:\\\/\\\/:9092/listeners=PLAINTEXT:\\\/\\\/:9092/\ns/log\\.dirs=\\\/tmp\\\/kafka-logs/log.dirs=\\\/tmp\\\/kafka-logs-0/" > config/server-0.sed
    echo "s/broker\\.id=0/broker.id=1/\ns/#listeners=PLAINTEXT:\\\/\\\/:9092/listeners=PLAINTEXT:\\\/\\\/:9093/\ns/log\\.dirs=\\\/tmp\\\/kafka-logs/log.dirs=\\\/tmp\\\/kafka-logs-1/" > config/server-1.sed
    echo "s/broker\\.id=0/broker.id=2/\ns/#listeners=PLAINTEXT:\\\/\\\/:9092/listeners=PLAINTEXT:\\\/\\\/:9094/\ns/log\\.dirs=\\\/tmp\\\/kafka-logs/log.dirs=\\\/tmp\\\/kafka-logs-2/" > config/server-2.sed

    cp config/server.properties config/server-0.properties
    cp config/server.properties config/server-1.properties
    cp config/server.properties config/server-2.properties

    sed -i -f config/server-0.sed config/server-0.properties
    sed -i -f config/server-1.sed config/server-1.properties
    sed -i -f config/server-2.sed config/server-2.properties

    # Start zookeeper & brokers with new configs
    echo "\nSTARTING ZOOKEEPER"
    bin/zookeeper-server-start.sh config/zookeeper.properties &

    echo "\nSTARTING BROKER 0"
    bin/kafka-server-start.sh config/server-0.properties &

    echo "\nSTARTING BROKER 1"
    bin/kafka-server-start.sh config/server-1.properties &

    echo "\nSTARTING BROKER 2"
    bin/kafka-server-start.sh config/server-2.properties &

    # Add the `test` topic
    echo "\nADDING THE 'test' TOPIC."
    bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 3 --partitions 1 --topic test

    echo "\n\nEverything is set up, time for 'vagrant ssh'."
  SHELL
end
